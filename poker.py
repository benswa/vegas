# Benson Liu
# bliu13@ucsc.edu
#
# CMPS 5P, Spring 2014
# Assignment 4
#
# This program identifies the cards in your hand, it deals out a large number
# hand and find statistics on the number of times each hand occurred
# (e.g.: number of times two of a kind occur, number of times three of a kind
# occur, number of times a flush occurs, etc).
# Suit ranking according to the game Bridge: Clubs, Diamonds, Hearts, Spades

__author__ = 'Ben'


from random import sample


# Declaring a list called deck to globally store the card values
deck = []


def generate_deck():
    """
    Must initially run this function to generate the full
    deck of cards.
    """
    full_deck = []
    for rank in '23456789TJQKA':
        for suit in 'CDHS':
            card = rank + suit
            full_deck.append(card)
    return full_deck


def count_rank(hand):
    """
    Output: Returns the number of cards in each rank.
    Example Hand: ['2C', 'QS', '5S', '6H', '5C']
    Ordered according to ranking on (1) and what is
    passed back is the quantity of each rank as shown
    in (2)
    (1)  [2, 3, 4, 5, 6, 7, 8, 9, T, J, Q, K, A]
    (2)  [1, 0, 0, 2, 1, 0, 0, 0, 0, 0, 1, 0, 0]
    """
    rank_quantity = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    index = 0
    for value in ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A']:
        for card in hand:
            if value in card:
                rank_quantity[index] += 1
        index += 1
    return rank_quantity


def count_suit(hand):
    """
    Output: Returns the number of cards in each suit.
    Example Hand: ['2C', 'QS', '5S', '6H', '5C']
    Ordered according to suit ranking on (1) and what is
    passed back is the quantity of each suit as shown
    in (2)
    (1)  [C, D, H, S]     Club, Diamond, Heart, Spade
    (2)  [2, 0, 1, 2]
    """
    suit_quantity = [0, 0, 0, 0]
    index = 0
    for value in ['C', 'D', 'H', 'S']:
        for card in hand:
            if value in card:
                suit_quantity[index] += 1
        index += 1
    return suit_quantity


def get_rank(card):
    """
    Output: Returns a number 0 through 12 with the rank of the card.
    Ordered according to ranking on (1) and what is passed back is
    the corresponding number of each rank as shown in (2)
    (1)  [2, 3, 4, 5, 6, 7, 8, 9, T, J,  Q,  K,  A]
    (2)  [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    *Note: (2) is pretty much the index of (1)'s values

    WARNING: You absolutely cannot pass in a List containing the card.
    It will always return the wrong answer. You must past in the actual
    card value. I.E. you cannot pass in something that is ['2C']
    and it must be '2C'. You can check this by printing out what you
    are passing in.
    """
    index = -1    # index set to -1 initially just in case no values match
    ranking = ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A']
    for rank in range(len(ranking)):
        if ranking[rank] in card:
            index = rank
            break
    return index


def get_suit(card):
    """
    Output: Returns a number 0 through 3 with the rank of the suit of
    the card. Ordered according to ranking on (1) and what is passed
    back is the corresponding number of each rank as shown in (2)
    (1)  [C, D, H, S]
    (2)  [0, 1, 2, 3]
    *Note: (2) is pretty much the index of (1)'s values

    WARNING: You absolutely cannot pass in a List containing the card.
    It will always return the wrong answer. You must past in the actual
    card value. I.E. you cannot pass in something that is ['2C']
    and it must be '2C'. You can check this by printing out what you
    are passing in.
    """
    index = -1    # index set to -1 initially just in case no values match
    suits = ['C', 'D', 'H', 'S']
    for rank in range(len(suits)):
        if suits[rank] in card:
            index = rank
            break
    return index


def rankpokerhand(hand):
    """
    This massive function returns an integer representation of kind/rank
    the hand is. By that, I mean how 'powerful' the hand is. As seen in
    first few lines in the function itself, the first 8 values are used
    to identify what kind of hand it is depending on the combination.
    The values between 9 and 19 actually should not represent anything
    because the lowest 'high card' is 7C and its number representation
    (also it's index in the deck) is 20.

    Values 9 and up are numbers that represent high cards if they do not
    fall under the first 8 combinations.
    """
    global deck

    # Declaring 'constant' values to return for certain combinations
    straightflush = 1
    fourofakind = 2
    fullhouse = 3
    flush = 4
    straight = 5
    threeofakind = 6
    twopair = 7
    pair = 8

    # Categorizing the hand to their ranks and suits
    suit_count = count_suit(hand)
    rank_count = count_rank(hand)

    # Determining if we have Flush or Straight Flush.
    # Example of Straight Flush: 4C, 5C, 8C, 6C, 7C
    # Example of Flush: 2H, TH, 8H, KH, 5H
    for suit_index in range(len(suit_count)):
        if suit_count[suit_index] is 5:
            # Recognized to be all same suit. Now to differentiate
            # between Flush and Straight Flush
            sequence_count = 0
            last_rank_index = -1
            for rank_index in range(-1, len(rank_count)):
                # Neat trick by starting range at -1 to check for [A2345]
                if rank_count[rank_index] is 1:
                    # Should never be possible that rank_count is
                    # greater than 1 if hand is a flush
                    if (rank_index - 1) is last_rank_index:
                        sequence_count += 1
                        if sequence_count is 5:
                            # Discovered Straight Flush
                            return straightflush
                    else:
                        sequence_count = 1
                    last_rank_index = rank_index
            # If not Straight Flush then it is a Flush
            return flush

    # Determining if we have a Straight
    # Example of a Straight: 5C, 9H, 8H, 6D, 7S
    sequence_count = 0
    last_rank_index = -1
    for rank_index in range(-1, len(rank_count)):
        # Neat trick by starting range at -1 to check for [A2345]
        if rank_count[rank_index] is 1:
            # Should never be possible that rank_count is
            # greater than 1 if hand is a Straight
            if (rank_index - 1) is last_rank_index:
                sequence_count += 1
                if sequence_count is 5:
                    # Discovered Straight
                    return straight
            else:
                sequence_count = 1
            last_rank_index = rank_index

    # Determining if we have Four of a Kind, Full House
    # or Three of a Kind.
    # Example of Four of a Kind: 5C, 5D, 8C, 5S, 5H
    # Have 4 of the same rank, disregard other card
    #
    # Example of Full House: AH, AS, 3C, 3H, AD
    # Have 3 of the same rank, other two cards being pairs
    #
    # Example of Three of a Kind: 7C, 9H, KH, 7D, 7S
    # Have 3 of the same rank, other two cards do not matter
    pair_found = False
    triple_found = False
    for rank_index in range(len(rank_count)):
        if rank_count[rank_index] is 4:
            # Discovered 4 cards in same rank
            return fourofakind
        if rank_count[rank_index] is 3:
            # Discovered 3 cards in same rank
            triple_found = True
        if rank_count[rank_index] is 2:
            # Discovered 2 cards in same rank
            pair_found = True
    # Dealing with Three of a Kind or Full House here
    if triple_found is True:
        if pair_found is True:
            return fullhouse
        return threeofakind

    # Determining if we have Two Pairs or a Pair
    # Example of Two Pair: 9C, JC, 3S, 9D, JS
    # Example of a Pair: TD, 4S, TH, 2H, AD
    pair_found = False
    two_pair_found = False
    for rank_index in range(len(rank_count)):
        if rank_count[rank_index] is 2:
            # Discovered 2 cards in same rank
            if pair_found is True:
                # If second pair is discovered
                two_pair_found = True
                break
            pair_found = True
    # Dealing with Three of a Kind or Full House here
    if pair_found is True:
        if two_pair_found is True:
            return twopair
        return pair

    # Determining highest card in hand if no combinations are found
    highest_card_rank = 0
    highest_card_suit = 0
    highest_card = ''
    for card in hand:
        card_rank = get_rank(card)
        card_suit = get_suit(card)
        if card_rank >= highest_card_rank:
            # Disregards cards with lower rank than highest card on record
            if (card_rank is highest_card_rank) and (card_suit > highest_card_suit):
                # If same rank, but higher suit
                highest_card_suit = card_suit
                highest_card = card
                continue
            if card_rank > highest_card_rank:
                # If card is higher rank, regardless of suit
                highest_card_rank = card_rank
                highest_card_suit = card_suit
                highest_card = card
                continue
    # Finding index of highest card in deck
    index = 0
    for card in deck:
        if highest_card in card:
            break
        index += 1
    return index


def hand_string(rank):
    """
    This function returns a value based on the rank of the hand, not the
    rank of the card. There is a difference since we are returning
    a string describing what type of rank (how powerful) the hand is.
    Will either return a string stating the combination or will return
    the highest card value in the hand. Values 9 and higher will return
    the highest value in hand and we can disregard the fact that we are
    not representing the first few values because it is impossible for those
    values to even be the highest. Furthermore, 7C is the lowest and its
    rank (index value) passed in is 20 so in reality, values 9 to 19
    are not even used. There are no checks preventing the string representation
    of those values between that range from being returned.
    """
    global deck
    if rank is 1:
        return 'straightflush'
    if rank is 2:
        return 'fourofakind'
    if rank is 3:
        return 'fullhouse'
    if rank is 4:
        return 'flush'
    if rank is 5:
        return 'straight'
    if rank is 6:
        return 'threeofakind'
    if rank is 7:
        return 'twopair'
    if rank is 8:
        return 'pair'
    else:
        # If rank of hand
        return deck[rank]


def monte_carlo(number_of_hands):
    """
    Monte Carlo simulation for the occurrences of each
    type of hand. The number of times the simulation is
    ran is specified in the parameter passed in. What it
    should return is a list of percentages. In the list
    it should contain the percentage of the following
    values to represent:

    [0] - 0 (Nothing, to be 'consistent' with rankpokerhand())
    [1] - straightflush
    [2] - fourofakind
    [3] - fullhouse
    [4] - flush
    [5] - straight
    [6] - threeofakind
    [7] - twopair
    [8] - pair
    [9] - highcard
    """
    global deck
    occurrences = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    percentage = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    for iteration in range(0, number_of_hands):
        generated_hand = sample(deck, 5)
        hand_ranking = rankpokerhand(generated_hand)
        if hand_ranking is 1:
            occurrences[1] += 1
        if hand_ranking is 2:
            occurrences[2] += 1
        if hand_ranking is 3:
            occurrences[3] += 1
        if hand_ranking is 4:
            occurrences[4] += 1
        if hand_ranking is 5:
            occurrences[5] += 1
        if hand_ranking is 6:
            occurrences[6] += 1
        if hand_ranking is 7:
            occurrences[7] += 1
        if hand_ranking is 8:
            occurrences[8] += 1
        if 9 <= hand_ranking < 20:
            # Values between here are not high card values.
            continue
        if hand_ranking >= 20:
            occurrences[9] += 1
    for index in range(0, len(percentage)):
        percentage[index] = (occurrences[index] / number_of_hands) * 100
    return percentage


if __name__ == '__main__':
    deck = generate_deck()
    percentages = monte_carlo(10000)
    for hand_type in range(1, len(percentages) - 1):
        # The range is between 1 and 8 in the array, 9 is displayed separately
        print('{0:18s} : {1:.2f}%'.format(hand_string(hand_type), percentages[hand_type]))
    print('{0:18s} : {1:.2f}%'.format('high card', percentages[9] - 1))